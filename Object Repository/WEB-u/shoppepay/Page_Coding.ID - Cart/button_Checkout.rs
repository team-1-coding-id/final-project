<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Checkout</name>
   <tag></tag>
   <elementGuidId>8a1b01a2-07f1-4ef7-82c2-99864c4312f5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#checkoutButton</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='checkoutButton']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>89e2fb62-dc50-4409-9a71-34e196991760</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>ffbb2e7e-aec7-4a29-9610-3c36142fb74d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>checkoutButton</value>
      <webElementGuid>2bff05ea-ff3d-4510-81a2-20e003d3327a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Checkout</value>
      <webElementGuid>a2b18ec2-4d52-4b64-9229-94dc61847fd7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;checkoutButton&quot;)</value>
      <webElementGuid>15a01d0f-c38c-489b-a52f-0c84c88f5e26</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='checkoutButton']</value>
      <webElementGuid>9f151018-9b49-48f1-bcd2-4767921281c9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='cartForm']/div[2]/div/button</value>
      <webElementGuid>e699c758-cfac-48dc-9310-a92cdc8750d3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Rp 85.000'])[2]/following::button[1]</value>
      <webElementGuid>2a229b7c-4a83-406c-89a0-d4b459052f81</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Total'])[1]/following::button[1]</value>
      <webElementGuid>d16d7366-c304-4ceb-9ab7-16d0830cc9a2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Apply'])[1]/preceding::button[1]</value>
      <webElementGuid>76249771-9016-4b89-bd9f-1179c3464129</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Riwayat Pembelian'])[1]/preceding::button[2]</value>
      <webElementGuid>6b5b8e35-b624-4172-a63f-3be1ebb0311e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/button</value>
      <webElementGuid>0cd92435-945d-47e4-9cb2-4eb198870b6d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and @id = 'checkoutButton' and (text() = 'Checkout' or . = 'Checkout')]</value>
      <webElementGuid>db3ec866-4d12-436a-a095-77d112dfc80a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
