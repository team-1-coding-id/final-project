<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Cancel                                   _4e9978</name>
   <tag></tag>
   <elementGuidId>4f4887d1-e4d1-4c16-a07c-8083a0ca61b0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#cancelButton</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='cancelButton']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>b87eb111-af60-4906-a071-94a93772deea</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>EVN21082300003/cancel</value>
      <webElementGuid>9cefc019-fb9e-4ff8-aa3a-605effcce177</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-danger btn-icon icon-left</value>
      <webElementGuid>507f43d4-663b-42b2-a4bb-9087b5c71e54</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>cancelButton</value>
      <webElementGuid>83c5c5b4-1636-450a-a89c-655a623436f4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Cancel
                                            Invoice</value>
      <webElementGuid>d2901e94-0379-4c1d-ad5a-46d765f2a83f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;cancelButton&quot;)</value>
      <webElementGuid>92545031-9bc1-490c-b27d-eb2db9369725</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[@id='cancelButton']</value>
      <webElementGuid>c0af9fa9-04a3-40b2-87ff-fbf31aa0d4e3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div[3]/section/div/div/div/div/div[3]/div[3]/div/ul/a[2]</value>
      <webElementGuid>bcae1b84-a1af-4906-8137-5cdcbd1b08d5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Cancel
                                            Invoice')]</value>
      <webElementGuid>6483763b-11d8-4823-928b-f9715b230ca3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Complete your payment here :'])[1]/following::a[2]</value>
      <webElementGuid>84e88cc1-6fa9-4214-a2ff-5a87a0f910da</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Invoice Date :'])[1]/following::a[2]</value>
      <webElementGuid>7d43e781-9191-4fa0-91ce-44156f1b44e1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Order Summary'])[1]/preceding::a[1]</value>
      <webElementGuid>81c0276b-e505-4afb-b5a8-9adcf60b8c1e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='All items here cannot be deleted'])[1]/preceding::a[1]</value>
      <webElementGuid>6a6bc4b3-c691-4cba-8881-b15f17fde320</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'EVN21082300003/cancel')]</value>
      <webElementGuid>a19071d6-9d71-42a9-9fc7-5fe786906a44</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a[2]</value>
      <webElementGuid>7be69833-5ab0-43f4-8531-41d815b08cf0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'EVN21082300003/cancel' and @id = 'cancelButton' and (text() = ' Cancel
                                            Invoice' or . = ' Cancel
                                            Invoice')]</value>
      <webElementGuid>7052d812-0bad-4b1c-9982-75cd44e70f98</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
