<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Login</name>
   <tag></tag>
   <elementGuidId>da0ffe49-43c2-4b6f-93b8-e433e3c848c0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#buttonLoginTrack</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='buttonLoginTrack']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>eec1a576-e7cb-4042-b5a0-f724dacc5abd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>buttonLoginTrack</value>
      <webElementGuid>928f3f15-edfd-4946-b225-9554a8658d5a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>submit</value>
      <webElementGuid>5fdff6df-567c-4369-aecf-29daf067bbd3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary btn-lg btn-block</value>
      <webElementGuid>67850907-a84c-4ca6-a265-990e87ef10a4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>4</value>
      <webElementGuid>04ecc711-a2af-4b61-8113-c23e2eac8046</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                        Login
                                                    </value>
      <webElementGuid>3a44a820-db21-410f-bd37-97a2b0e908c8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;buttonLoginTrack&quot;)</value>
      <webElementGuid>1d0e29b9-857f-4fa7-85e5-0ceb88e38a65</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='buttonLoginTrack']</value>
      <webElementGuid>f5bce2d5-1e72-401d-b228-3bffc5f6d575</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Email'])[1]/following::button[1]</value>
      <webElementGuid>6f04ad70-fcd9-43bd-9f04-40f637d1c86b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lupa kata sandi ?'])[1]/preceding::button[2]</value>
      <webElementGuid>ba48fb3f-236d-4ef9-9dbf-ad86975b6453</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/button</value>
      <webElementGuid>a1e58c9a-28b8-45ed-a7a3-538a6b36dd13</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@id = 'buttonLoginTrack' and @type = 'submit' and (text() = '
                                                        Login
                                                    ' or . = '
                                                        Login
                                                    ')]</value>
      <webElementGuid>801101b0-91bb-4145-85b4-ef9b40251956</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
