<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Confirm</name>
   <tag></tag>
   <elementGuidId>611bff78-5faf-4a33-a1d5-d747e5cc4d6c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#confirm</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='confirm']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>cc10d915-0bcd-4aa0-9d05-7bf207a5599f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>6eadf227-de77-49d1-95b2-2b99fd5e33d4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>confirm</value>
      <webElementGuid>74f40be6-7bf4-40b7-a6d2-633a7a8cb701</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary btn-order btn-block</value>
      <webElementGuid>79aa38cc-45f4-487a-aead-2c850f79d7c4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Confirm</value>
      <webElementGuid>fe8a51a6-8d23-4387-98ca-684aa883710f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;confirm&quot;)</value>
      <webElementGuid>b3010a1d-380d-4786-bf75-5e42100bfabc</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='confirm']</value>
      <webElementGuid>eaca41c0-d42d-4e9b-8cac-7fef52bad82d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='modalform']/button</value>
      <webElementGuid>07ac5618-d670-4429-9719-9d2388467a65</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Transfer QR Code / Bank (cek otomatis)'])[1]/following::button[1]</value>
      <webElementGuid>bdbdb96f-4828-47f6-8862-581db5108a54</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Information'])[1]/preceding::button[1]</value>
      <webElementGuid>c7fe6829-9cbc-47e7-850b-fdf95721eef8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ok'])[1]/preceding::button[2]</value>
      <webElementGuid>80a02687-3a74-4e41-91cd-acccdfb7150f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Confirm']/parent::*</value>
      <webElementGuid>e36fc0d5-5720-4199-b03e-5879cdbca8f8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/form/button</value>
      <webElementGuid>d6e81205-4d52-4424-a903-713610519d73</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and @id = 'confirm' and (text() = 'Confirm' or . = 'Confirm')]</value>
      <webElementGuid>6a4ea88c-a84e-45e4-80aa-53d3f69371bc</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
