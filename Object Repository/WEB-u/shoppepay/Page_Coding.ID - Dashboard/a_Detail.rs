<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Detail</name>
   <tag></tag>
   <elementGuidId>54ba0b55-696a-4341-be6a-5b4e47191aa9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//table[@id='tableCourse']/tbody/tr[4]/td[5]/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>fdfb7ad3-4f6f-46f1-9594-1f5be5f96094</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>invoice/EVN21082300003</value>
      <webElementGuid>7ae690fc-4009-4a9b-a7d9-9fa9f29b2d3e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-info</value>
      <webElementGuid>0c89febf-0cb5-4f38-ac16-7eae0db29cce</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Detail</value>
      <webElementGuid>af4de174-a2ec-49a1-8714-23a020124d26</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;tableCourse&quot;)/tbody[1]/tr[@class=&quot;even&quot;]/td[5]/a[@class=&quot;btn btn-info&quot;]</value>
      <webElementGuid>86d13901-a023-4cc5-80cc-a465bdcc9b08</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='tableCourse']/tbody/tr[4]/td[5]/a</value>
      <webElementGuid>3e72b397-cb91-4fdd-9317-6dcc8bcb89a6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>(//a[contains(text(),'Detail')])[4]</value>
      <webElementGuid>20dbf585-87ec-4258-9d1a-4c4890b8269d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Unpaid'])[1]/following::a[1]</value>
      <webElementGuid>fa7dc6fc-cbf1-4a3b-825a-2eab16c3b3a5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='EVN21082300003'])[1]/following::a[1]</value>
      <webElementGuid>e02925a6-c038-4819-ae5e-81da390530e1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='EVN21082300002'])[1]/preceding::a[1]</value>
      <webElementGuid>df975f5c-c7c1-43c0-be34-ed68bfdda594</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cancel by User'])[4]/preceding::a[1]</value>
      <webElementGuid>e58df54e-a82b-49c8-8450-92611d27e64c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'invoice/EVN21082300003')]</value>
      <webElementGuid>9bd4a62b-02b7-4701-8af0-0acf2642358f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[4]/td[5]/a</value>
      <webElementGuid>3555ea17-3ff3-4128-80fb-05c964e94bc7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'invoice/EVN21082300003' and (text() = 'Detail' or . = 'Detail')]</value>
      <webElementGuid>45c7fd49-3e63-43ae-9690-e259e8e4d5c4</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
