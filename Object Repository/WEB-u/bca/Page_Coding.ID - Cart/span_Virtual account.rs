<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Virtual account</name>
   <tag></tag>
   <elementGuidId>c440bc7b-3b2b-4ed8-af10-d0df525a5912</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='application']/div/div/div[2]/div/div/div/div/div/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.collapsible-payment--multiple__title > span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>2cf7d508-53a2-40bf-9c71-465f28ef635b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Virtual account</value>
      <webElementGuid>45fd7fa9-23d3-45a6-9ad7-899932f8d4b8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;application&quot;)/div[@class=&quot;page-container scroll&quot;]/div[@class=&quot;payment-container-list&quot;]/div[2]/div[@class=&quot;collapsible-payment&quot;]/div[@class=&quot;collapsible collapsible-payment--multiple&quot;]/div[@class=&quot;collapse-button collapsible-payment--multiple&quot;]/div[@class=&quot;collapsible-payment--multiple__body&quot;]/div[@class=&quot;collapsible-payment--multiple__title&quot;]/span[1]</value>
      <webElementGuid>11ac9fe7-f254-4d75-864e-8df16ac8bf02</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/WEB-u/bca/Page_Coding.ID - Cart/iframe_concat(id(, , snap-midtrans, , ))_po_adc828</value>
      <webElementGuid>ad42525f-6ee3-4bf2-ad63-21c993314653</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='application']/div/div/div[2]/div/div/div/div/div/span</value>
      <webElementGuid>9d8254aa-fa03-4b7c-b2ce-ed9ae79b76ef</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='All payment methods'])[1]/following::span[1]</value>
      <webElementGuid>ef014f50-4773-41bc-a5cd-4ba1dd7acaef</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Other banks'])[1]/preceding::span[1]</value>
      <webElementGuid>8c12ded9-38f0-4d8f-952e-2649b08e69dc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Credit/debit card'])[1]/preceding::span[2]</value>
      <webElementGuid>9c16ddca-e44e-4f7c-ba39-8d76ca4fe73e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Virtual account']/parent::*</value>
      <webElementGuid>3ba782f5-0b50-4144-96f4-fff50860ffbc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div/div/div/span</value>
      <webElementGuid>dce751eb-bdbe-4f05-ad45-9b895e44b3c6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Virtual account' or . = 'Virtual account')]</value>
      <webElementGuid>7ae4649c-c1a7-4325-a8b5-6f9540ef523d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
