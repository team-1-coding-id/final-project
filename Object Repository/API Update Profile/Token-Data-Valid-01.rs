<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Token-Data-Valid-01</name>
   <tag></tag>
   <elementGuidId>639b3af4-2a4c-4558-88d0-3d2e352f98a5</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <authorizationRequest>
      <authorizationInfo>
         <entry>
            <key>bearerToken</key>
            <value>eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIzIiwianRpIjoiMjBjNzhlMjNmYzZiMjY5ZmRmZjkwYjNhNDI3N2I2YzQ2MzExMTZkMDNkMTQ3OTFlMGMwMDVhN2FiM2I3MjcxOGZjZjlkNGQ1NGVlYzk5MDUiLCJpYXQiOjE2OTE5ODkyMzYuNDgyOTc3LCJuYmYiOjE2OTE5ODkyMzYuNDgyOTgsImV4cCI6MTcyMzYxMTYzNi40ODAxNDcsInN1YiI6IjY0Iiwic2NvcGVzIjpbXX0.WiqHQ118quHqO9g3Z-yno-JQ1_oCzpO8wCD2kFYMTRuW-6MfW0eNhgCLQV67cqxPYzsWAnkbA6UgFgkucIhSAisQQCntPQp2DLYwAvblPLFttZh3qSvjXXgOc9jqHEwKLDk6T4vvsC7kUrn84Lyk0kRPYznx8rgLIGY1qzCGzBQLo9STJYSnM-PUGE9RIZnmsvw2ykEjsm1e7dY5oaaP33owBmgVDMxsbK5Lqj8e9YQyrIu5jIioKZgTs-fU7RsXvmsIXuAhjC6KRdcoitY-h72-OAsJv57g92741RyMNzpA4WV1emrJFEd0-rNbFVwoyLJVV0rd3WNmkt-hzm25DTG6SsxRnaRksEN4w4aKLjXRPtOGIhSdAaL17BGFdGHMaRr4vpnZmlm2CeqYoTGIhRaWe1x1dMz56OMLbvtdfSl1Mtq5oyTR-R-_Y2CJgSIupU6AO6SZAzpDJs2OtXoThr2_4LVFxVt64TMhAtFU8r2nc5KlXaP1yonJICX25BCZvqUSNWW0gH8XhmMcAzWWrlL2TPOZQo7yNhMhPAnSkScPPm7CGRCTXKijGAVtUgrqtaO-C0wpQ9zxd9bRb4hxodS2bVrhG7560_Cr9bhDQ46GitW739CFWQngMlVIh1zNb3nlQvFzTlDOrA8lQYgvRmRufNHtKBu0K1lI6EiQdFo</value>
         </entry>
      </authorizationInfo>
      <authorizationType>Bearer</authorizationType>
   </authorizationRequest>
   <autoUpdateContent>false</autoUpdateContent>
   <connectionTimeout>0</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;contentType&quot;: &quot;multipart/form-data&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;,
  &quot;parameters&quot;: [
    {
      &quot;name&quot;: &quot;name&quot;,
      &quot;value&quot;: &quot;Havizh123&quot;,
      &quot;type&quot;: &quot;Text&quot;,
      &quot;contentType&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;whatsapp&quot;,
      &quot;value&quot;: &quot;&quot;,
      &quot;type&quot;: &quot;Text&quot;,
      &quot;contentType&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;birth_date&quot;,
      &quot;value&quot;: &quot;12-03-2002&quot;,
      &quot;type&quot;: &quot;Text&quot;,
      &quot;contentType&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;photo&quot;,
      &quot;value&quot;: &quot;E:\\CODING.ID\\screenshot.png&quot;,
      &quot;type&quot;: &quot;File&quot;,
      &quot;contentType&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;bio&quot;,
      &quot;value&quot;: &quot;dev&quot;,
      &quot;type&quot;: &quot;Text&quot;,
      &quot;contentType&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;position&quot;,
      &quot;value&quot;: &quot;eng&quot;,
      &quot;type&quot;: &quot;Text&quot;,
      &quot;contentType&quot;: &quot;&quot;
    }
  ]
}</httpBodyContent>
   <httpBodyType>form-data</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>multipart/form-data</value>
      <webElementGuid>bb728422-d4b5-4e64-afa8-29dfb9b21c6d</webElementGuid>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIzIiwianRpIjoiMjBjNzhlMjNmYzZiMjY5ZmRmZjkwYjNhNDI3N2I2YzQ2MzExMTZkMDNkMTQ3OTFlMGMwMDVhN2FiM2I3MjcxOGZjZjlkNGQ1NGVlYzk5MDUiLCJpYXQiOjE2OTE5ODkyMzYuNDgyOTc3LCJuYmYiOjE2OTE5ODkyMzYuNDgyOTgsImV4cCI6MTcyMzYxMTYzNi40ODAxNDcsInN1YiI6IjY0Iiwic2NvcGVzIjpbXX0.WiqHQ118quHqO9g3Z-yno-JQ1_oCzpO8wCD2kFYMTRuW-6MfW0eNhgCLQV67cqxPYzsWAnkbA6UgFgkucIhSAisQQCntPQp2DLYwAvblPLFttZh3qSvjXXgOc9jqHEwKLDk6T4vvsC7kUrn84Lyk0kRPYznx8rgLIGY1qzCGzBQLo9STJYSnM-PUGE9RIZnmsvw2ykEjsm1e7dY5oaaP33owBmgVDMxsbK5Lqj8e9YQyrIu5jIioKZgTs-fU7RsXvmsIXuAhjC6KRdcoitY-h72-OAsJv57g92741RyMNzpA4WV1emrJFEd0-rNbFVwoyLJVV0rd3WNmkt-hzm25DTG6SsxRnaRksEN4w4aKLjXRPtOGIhSdAaL17BGFdGHMaRr4vpnZmlm2CeqYoTGIhRaWe1x1dMz56OMLbvtdfSl1Mtq5oyTR-R-_Y2CJgSIupU6AO6SZAzpDJs2OtXoThr2_4LVFxVt64TMhAtFU8r2nc5KlXaP1yonJICX25BCZvqUSNWW0gH8XhmMcAzWWrlL2TPOZQo7yNhMhPAnSkScPPm7CGRCTXKijGAVtUgrqtaO-C0wpQ9zxd9bRb4hxodS2bVrhG7560_Cr9bhDQ46GitW739CFWQngMlVIh1zNb3nlQvFzTlDOrA8lQYgvRmRufNHtKBu0K1lI6EiQdFo</value>
      <webElementGuid>0ac62e38-59fc-4c6f-aa19-ee01c86f4c23</webElementGuid>
   </httpHeaderProperties>
   <katalonVersion>8.6.5</katalonVersion>
   <maxResponseSize>0</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>https://demo-app.online/api/updateprofile</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>0</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
