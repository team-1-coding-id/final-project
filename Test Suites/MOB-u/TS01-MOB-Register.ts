<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TS01-MOB-Register</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>2ac7acea-2bfa-41ff-91e4-3b8dcb73200d</testSuiteGuid>
   <testCaseLink>
      <guid>7fdcd8f6-819d-4b88-b429-8fc77c37a220</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MOB-u/MOB - S02 - Register/TC-REG-01 - Register with valid data entry</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>bba66115-5087-40bd-8ab4-69de61eea7b7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MOB-u/MOB - S02 - Register/TC-REG-02- Resend the verification link</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>1c189ab7-66c0-40df-8a0c-56022baa1a69</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MOB-u/MOB - S02 - Register/TC-REG-03 - Register by entering special characters in the name column</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>0faf74a7-fd49-4d9e-92e0-2d34c7db692d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MOB-u/MOB - S02 - Register/TC-REG-04 - Register by entering numbers in the name column</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>0bc79ed2-beae-4d22-b5bf-4f0d7c6c9e14</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MOB-u/MOB - S02 - Register/TC-REG-05 - Register without entering the symbol character in the email field</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>5a5c15ae-8b63-4dc1-95ef-f0eea95b3fd5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MOB-u/MOB - S02 - Register/TC-REG-06 - Register without entering the letter after the character (dot) in the email column</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>61643570-92f2-410d-81b5-382f9e737155</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MOB-u/MOB - S02 - Register/TC-REG-07 - Register with a Whatsapp number less than 9 characters</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>687fc5f6-7586-4c60-870d-97c9f8e27bc6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MOB-u/MOB - S02 - Register/TC-REG-08 - Register with a password of less than 8 characters</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
