<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Coding Bootcamp            Tech Talent _a677a2</name>
   <tag></tag>
   <elementGuidId>896513a7-f21e-4ffa-8439-be674fd7dc79</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.containerHeader</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='main-body-homepage']/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>de7e56ea-90e7-4ad4-8598-05d9d4001b3e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>containerHeader</value>
      <webElementGuid>945132d3-46b1-49f2-99bb-de6cd4933322</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>x-data</name>
      <type>Main</type>
      <value>{}</value>
      <webElementGuid>f8d98e53-0035-44ac-890b-273710b4319b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
        Coding Bootcamp
            Tech Talent Berkualitas

        Bootcamp untuk semua,
            dengan
            background IT maupun Non-IT.
            Pilih programnya dan dapatkan jaminan kerja.
                    
                
                    
                    
                        Quality Assurance Engineer Class
                                                    
                                Jadi QA Engineer hanya dalam 2 bulan dengan Jaminan Kerja dan Beasiswa*.
                                            

                
                                    
                        Daftar
                            Sekarang
                    
                            
                    
                
                    
                    
                        Fullstack Engineer Class
                                                    
                                Jadi Fullstack Engineer hanya dalam 2 bulan dengan Jaminan Kerja dan Beasiswa*.
                                            

                
                                    
                        Daftar
                            Sekarang
                    
                            
                



        
            
                
                    Belum kerja?
                        Tidak usah bayar!
                    Jaminan uang kembali 100% jika kamu tidak
                        mendapatkan
                        pekerjaan untuk Upfront Payment dan tidak perlu
                        membayar sampai kamu dapat kerja dengan metode ISA
                

                
                    Belajar dari Praktisi
                    Coach di Coding.ID merupakan praktisi yang telah
                        berpengalaman di bidangnya loh jadi tenang aja kamu berada di tangan yang tepat kok
                

                
                    Akses ke semua
                        Event &amp; Course
                    Peserta maupun alumni akan mendapatkan privilage
                        untuk
                        mengakses semua semua Events maupun Online Course di Coding.ID secara gratis!
                

                
                    Belajar dari Nol sampai Jago
                    Kamu masih awam? Tenang aja Coding.ID akan
                        membangun
                        kemampuan programing kamu secara mandiri dengan cara-cara yang sangat sederhana.
                
            


            
                
                    
                
                
                    
                
                
                    
                
                
                    
                
            

        

    </value>
      <webElementGuid>bf27844b-d75a-44e1-960a-c7d70eddaa08</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main-body-homepage&quot;)/div[@class=&quot;containerHeader&quot;]</value>
      <webElementGuid>af558fca-0fdb-4cb1-8aae-1e00ba559895</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='main-body-homepage']/div</value>
      <webElementGuid>4257457e-1265-44c8-94a4-de2bd20e821a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div[4]/div</value>
      <webElementGuid>620d6ab4-6278-41d2-a8f4-4e4016d06613</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
        Coding Bootcamp
            Tech Talent Berkualitas

        Bootcamp untuk semua,
            dengan
            background IT maupun Non-IT.
            Pilih programnya dan dapatkan jaminan kerja.
                    
                
                    
                    
                        Quality Assurance Engineer Class
                                                    
                                Jadi QA Engineer hanya dalam 2 bulan dengan Jaminan Kerja dan Beasiswa*.
                                            

                
                                    
                        Daftar
                            Sekarang
                    
                            
                    
                
                    
                    
                        Fullstack Engineer Class
                                                    
                                Jadi Fullstack Engineer hanya dalam 2 bulan dengan Jaminan Kerja dan Beasiswa*.
                                            

                
                                    
                        Daftar
                            Sekarang
                    
                            
                



        
            
                
                    Belum kerja?
                        Tidak usah bayar!
                    Jaminan uang kembali 100% jika kamu tidak
                        mendapatkan
                        pekerjaan untuk Upfront Payment dan tidak perlu
                        membayar sampai kamu dapat kerja dengan metode ISA
                

                
                    Belajar dari Praktisi
                    Coach di Coding.ID merupakan praktisi yang telah
                        berpengalaman di bidangnya loh jadi tenang aja kamu berada di tangan yang tepat kok
                

                
                    Akses ke semua
                        Event &amp; Course
                    Peserta maupun alumni akan mendapatkan privilage
                        untuk
                        mengakses semua semua Events maupun Online Course di Coding.ID secara gratis!
                

                
                    Belajar dari Nol sampai Jago
                    Kamu masih awam? Tenang aja Coding.ID akan
                        membangun
                        kemampuan programing kamu secara mandiri dengan cara-cara yang sangat sederhana.
                
            


            
                
                    
                
                
                    
                
                
                    
                
                
                    
                
            

        

    ' or . = '
        Coding Bootcamp
            Tech Talent Berkualitas

        Bootcamp untuk semua,
            dengan
            background IT maupun Non-IT.
            Pilih programnya dan dapatkan jaminan kerja.
                    
                
                    
                    
                        Quality Assurance Engineer Class
                                                    
                                Jadi QA Engineer hanya dalam 2 bulan dengan Jaminan Kerja dan Beasiswa*.
                                            

                
                                    
                        Daftar
                            Sekarang
                    
                            
                    
                
                    
                    
                        Fullstack Engineer Class
                                                    
                                Jadi Fullstack Engineer hanya dalam 2 bulan dengan Jaminan Kerja dan Beasiswa*.
                                            

                
                                    
                        Daftar
                            Sekarang
                    
                            
                



        
            
                
                    Belum kerja?
                        Tidak usah bayar!
                    Jaminan uang kembali 100% jika kamu tidak
                        mendapatkan
                        pekerjaan untuk Upfront Payment dan tidak perlu
                        membayar sampai kamu dapat kerja dengan metode ISA
                

                
                    Belajar dari Praktisi
                    Coach di Coding.ID merupakan praktisi yang telah
                        berpengalaman di bidangnya loh jadi tenang aja kamu berada di tangan yang tepat kok
                

                
                    Akses ke semua
                        Event &amp; Course
                    Peserta maupun alumni akan mendapatkan privilage
                        untuk
                        mengakses semua semua Events maupun Online Course di Coding.ID secara gratis!
                

                
                    Belajar dari Nol sampai Jago
                    Kamu masih awam? Tenang aja Coding.ID akan
                        membangun
                        kemampuan programing kamu secara mandiri dengan cara-cara yang sangat sederhana.
                
            


            
                
                    
                
                
                    
                
                
                    
                
                
                    
                
            

        

    ')]</value>
      <webElementGuid>704612fa-bfc3-4998-a7e5-333fdb3d37be</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
