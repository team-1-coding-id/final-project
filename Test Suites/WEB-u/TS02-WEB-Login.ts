<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TS02-WEB-Login</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>bbe12c93-7904-4ce4-92bd-86dfae865b2b</testSuiteGuid>
   <testCaseLink>
      <guid>27ce2e66-903f-4c3b-9be5-3abcccf346b7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEB-u/WEB - S02 - Login/TC-LOG-01 - Login with valid username and password</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>09afec60-0071-4bb4-ae15-b54cbb3e78a3</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Login-u/data-login</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>09afec60-0071-4bb4-ae15-b54cbb3e78a3</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>email</value>
         <variableId>3988ad2f-2cb1-4bd4-82f1-00e10ab47a06</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>09afec60-0071-4bb4-ae15-b54cbb3e78a3</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>5c919957-32ee-402e-b411-1f5f316c59f9</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>8cb43281-1e04-434c-8bc8-296b974a3db2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEB-u/WEB - S02 - Login/TC-LOG-02 - Login with valid username and invalid password</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>717b3a49-4fb6-4276-b2d8-9deaaf166e1e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEB-u/WEB - S02 - Login/TC-LOG-03 - Login with invalid username and valid password</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>3ee42956-bc52-4e09-9497-443e369caff5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEB-u/WEB - S02 - Login/TC-LOG-04 - Login with valid username and none password</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>0c5636f4-0be1-4ee8-9d11-e3c95833f510</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEB-u/WEB - S02 - Login/TC-LOG-05 - Login with none username and valid password</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>da613005-9dd9-4fe3-9e4d-8612f8de6289</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEB-u/WEB - S02 - Login/TC-LOG-06 - Login with invalid username and password</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>4ce05438-819f-458a-97fa-f64683ed5139</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEB-u/WEB - S02 - Login/TC-LOG-07 - Login without everything</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
