<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Total Pembayaran_payment_method</name>
   <tag></tag>
   <elementGuidId>94cd0cae-8bf9-47d9-a64a-ce41d4ddccc7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#bank</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='bank']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>e15235bf-1b10-4f78-8dfa-d3f755086bab</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>radio</value>
      <webElementGuid>7ccc47e3-ba94-43ea-bf9e-5fd5780656da</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control-inline payment</value>
      <webElementGuid>bad27d18-8b18-41e5-9344-47b727ab44ff</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>payment_method</value>
      <webElementGuid>6708b741-9cf9-4f0a-866c-d5f007351158</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>e-wallet</value>
      <webElementGuid>bb79b23a-0d9f-4719-a428-a1b7d57426e9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>bank</value>
      <webElementGuid>9c64245e-3e37-47df-9049-24c052dc4f5b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;bank&quot;)</value>
      <webElementGuid>3cf023d1-1ca3-4a67-b337-2ed7e901d008</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='bank']</value>
      <webElementGuid>26db342d-5e9a-40fd-bac6-3eb39fb790b4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='modalform']/table[2]/tbody/tr/td/input</value>
      <webElementGuid>7ac1f354-ec54-4dd5-89da-64a2fe374496</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td/input</value>
      <webElementGuid>1cbc82c8-5cc7-4376-9ac4-b1177e1bb796</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'radio' and @name = 'payment_method' and @id = 'bank']</value>
      <webElementGuid>eb9d29fd-07b1-429f-8dc1-bc71491074a8</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
