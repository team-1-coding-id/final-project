<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Coding Bootcamp            Tech Talent _a677a2</name>
   <tag></tag>
   <elementGuidId>66c6f355-bd26-4876-a988-34b8410d60af</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.containerHeader</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='main-body-homepage']/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>bab2af6b-cfde-445c-8747-7134acf56a9b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>containerHeader</value>
      <webElementGuid>3b7bde03-2f74-4360-ae39-606855958ac4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>x-data</name>
      <type>Main</type>
      <value>{}</value>
      <webElementGuid>265b33cd-8a98-499c-8a13-cfa775c02ab9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
        Coding Bootcamp
            Tech Talent Berkualitas

        Bootcamp untuk semua,
            dengan
            background IT maupun Non-IT.
            Pilih programnya dan dapatkan jaminan kerja.
                    
                
                    
                    
                        Quality Assurance Engineer Class
                                                    
                                Jadi QA Engineer hanya dalam 2 bulan dengan Jaminan Kerja dan Beasiswa*.
                                            

                
                                    
                        Daftar
                            Sekarang
                    
                            
                    
                
                    
                    
                        Fullstack Engineer Class
                                                    
                                Jadi Fullstack Engineer hanya dalam 2 bulan dengan Jaminan Kerja dan Beasiswa*.
                                            

                
                                    
                        Daftar
                            Sekarang
                    
                            
                



        
            
                
                    Belum kerja?
                        Tidak usah bayar!
                    Jaminan uang kembali 100% jika kamu tidak
                        mendapatkan
                        pekerjaan untuk Upfront Payment dan tidak perlu
                        membayar sampai kamu dapat kerja dengan metode ISA
                

                
                    Belajar dari Praktisi
                    Coach di Coding.ID merupakan praktisi yang telah
                        berpengalaman di bidangnya loh jadi tenang aja kamu berada di tangan yang tepat kok
                

                
                    Akses ke semua
                        Event &amp; Course
                    Peserta maupun alumni akan mendapatkan privilage
                        untuk
                        mengakses semua semua Events maupun Online Course di Coding.ID secara gratis!
                

                
                    Belajar dari Nol sampai Jago
                    Kamu masih awam? Tenang aja Coding.ID akan
                        membangun
                        kemampuan programing kamu secara mandiri dengan cara-cara yang sangat sederhana.
                
            


            
                
                    
                
                
                    
                
                
                    
                
                
                    
                
            

        

    </value>
      <webElementGuid>3a576b99-2070-4286-ab9a-77c473aea3aa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main-body-homepage&quot;)/div[@class=&quot;containerHeader&quot;]</value>
      <webElementGuid>38f2458e-c9c8-4a77-b517-ad52a860420b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='main-body-homepage']/div</value>
      <webElementGuid>fb8cc436-8caf-4776-a517-d540e77a95d0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div[4]/div</value>
      <webElementGuid>01a87540-196f-48ea-a742-d6b8300ff1b5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
        Coding Bootcamp
            Tech Talent Berkualitas

        Bootcamp untuk semua,
            dengan
            background IT maupun Non-IT.
            Pilih programnya dan dapatkan jaminan kerja.
                    
                
                    
                    
                        Quality Assurance Engineer Class
                                                    
                                Jadi QA Engineer hanya dalam 2 bulan dengan Jaminan Kerja dan Beasiswa*.
                                            

                
                                    
                        Daftar
                            Sekarang
                    
                            
                    
                
                    
                    
                        Fullstack Engineer Class
                                                    
                                Jadi Fullstack Engineer hanya dalam 2 bulan dengan Jaminan Kerja dan Beasiswa*.
                                            

                
                                    
                        Daftar
                            Sekarang
                    
                            
                



        
            
                
                    Belum kerja?
                        Tidak usah bayar!
                    Jaminan uang kembali 100% jika kamu tidak
                        mendapatkan
                        pekerjaan untuk Upfront Payment dan tidak perlu
                        membayar sampai kamu dapat kerja dengan metode ISA
                

                
                    Belajar dari Praktisi
                    Coach di Coding.ID merupakan praktisi yang telah
                        berpengalaman di bidangnya loh jadi tenang aja kamu berada di tangan yang tepat kok
                

                
                    Akses ke semua
                        Event &amp; Course
                    Peserta maupun alumni akan mendapatkan privilage
                        untuk
                        mengakses semua semua Events maupun Online Course di Coding.ID secara gratis!
                

                
                    Belajar dari Nol sampai Jago
                    Kamu masih awam? Tenang aja Coding.ID akan
                        membangun
                        kemampuan programing kamu secara mandiri dengan cara-cara yang sangat sederhana.
                
            


            
                
                    
                
                
                    
                
                
                    
                
                
                    
                
            

        

    ' or . = '
        Coding Bootcamp
            Tech Talent Berkualitas

        Bootcamp untuk semua,
            dengan
            background IT maupun Non-IT.
            Pilih programnya dan dapatkan jaminan kerja.
                    
                
                    
                    
                        Quality Assurance Engineer Class
                                                    
                                Jadi QA Engineer hanya dalam 2 bulan dengan Jaminan Kerja dan Beasiswa*.
                                            

                
                                    
                        Daftar
                            Sekarang
                    
                            
                    
                
                    
                    
                        Fullstack Engineer Class
                                                    
                                Jadi Fullstack Engineer hanya dalam 2 bulan dengan Jaminan Kerja dan Beasiswa*.
                                            

                
                                    
                        Daftar
                            Sekarang
                    
                            
                



        
            
                
                    Belum kerja?
                        Tidak usah bayar!
                    Jaminan uang kembali 100% jika kamu tidak
                        mendapatkan
                        pekerjaan untuk Upfront Payment dan tidak perlu
                        membayar sampai kamu dapat kerja dengan metode ISA
                

                
                    Belajar dari Praktisi
                    Coach di Coding.ID merupakan praktisi yang telah
                        berpengalaman di bidangnya loh jadi tenang aja kamu berada di tangan yang tepat kok
                

                
                    Akses ke semua
                        Event &amp; Course
                    Peserta maupun alumni akan mendapatkan privilage
                        untuk
                        mengakses semua semua Events maupun Online Course di Coding.ID secara gratis!
                

                
                    Belajar dari Nol sampai Jago
                    Kamu masih awam? Tenang aja Coding.ID akan
                        membangun
                        kemampuan programing kamu secara mandiri dengan cara-cara yang sangat sederhana.
                
            


            
                
                    
                
                
                    
                
                
                    
                
                
                    
                
            

        

    ')]</value>
      <webElementGuid>8d7bdf2d-ffc5-4577-8230-8e1778ee9c04</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
