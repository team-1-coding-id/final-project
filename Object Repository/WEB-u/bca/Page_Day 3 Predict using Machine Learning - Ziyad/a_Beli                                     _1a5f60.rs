<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Beli                                     _1a5f60</name>
   <tag></tag>
   <elementGuidId>3b2425e3-2276-4046-bb8e-759a5af0ac3c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#addCart</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='addCart']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>2bf46e1c-782a-4356-9377-ea7134f79c87</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>addCart</value>
      <webElementGuid>874f1b76-45c5-4f03-84ca-bc25e112112c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary btn-lg</value>
      <webElementGuid>37231d03-2a3a-429e-bf88-4ae99f99f44f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Beli
                                                    Tiket</value>
      <webElementGuid>c5bdcc85-60bd-4890-8615-5cbb98d880cf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;addCart&quot;)</value>
      <webElementGuid>d26055d7-36b3-42c6-9cbe-2842053478e8</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[@id='addCart']</value>
      <webElementGuid>437f3e92-4b99-4991-b9d9-b836df7ff34e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='list-button']/a</value>
      <webElementGuid>2b5d9bef-6d65-4213-8d51-f9880df45470</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Beli
                                                    Tiket')]</value>
      <webElementGuid>aec5215b-a267-4ef7-ae9a-9c4d88fd1d72</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Detik'])[1]/following::a[1]</value>
      <webElementGuid>df1473a5-edba-4e09-8f70-47b38eac4303</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Menit'])[1]/following::a[1]</value>
      <webElementGuid>9e233e44-6e86-48de-b648-3d64cf2335d2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Event lain yang banyak diminati'])[1]/preceding::a[1]</value>
      <webElementGuid>a0f57e53-3c97-44dc-a4c5-907af72aa3e1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Day 4: Workshop'])[1]/preceding::a[1]</value>
      <webElementGuid>23932c24-cebb-4f48-bca1-b2629096d5be</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//aside/div[2]/ul/li[6]/a</value>
      <webElementGuid>d7c50528-4d77-4941-94d5-fe203ed74dc9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@id = 'addCart' and (text() = 'Beli
                                                    Tiket' or . = 'Beli
                                                    Tiket')]</value>
      <webElementGuid>de5dcc5a-e5f4-48be-90ea-1141a097f211</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
