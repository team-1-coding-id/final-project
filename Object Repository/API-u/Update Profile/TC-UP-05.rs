<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>TC-UP-05</name>
   <tag></tag>
   <elementGuidId>c7ef8e7c-7d67-463e-bd4f-31f0bfc0c02b</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <authorizationRequest>
      <authorizationInfo>
         <entry>
            <key>bearerToken</key>
            <value>eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIzIiwianRpIjoiNTNiMTA3YTAxMzkyZTU3ZTRjY2E5MTAwNzUzZDkzMjBjNTIyMDQ5ZDAxZjdlYmViNDVmMjVlZGRjNjhmMDVhNjU1NzlhYjYwODE2ODdhMzciLCJpYXQiOjE2OTI1ODg5ODIuMjQ4ODQxLCJuYmYiOjE2OTI1ODg5ODIuMjQ4ODQzLCJleHAiOjE3MjQyMTEzODIuMjQ3NjM5LCJzdWIiOiI2NCIsInNjb3BlcyI6W119.JX_LkPVLF4iQ3hjSzVw9DjYZpG1LHCBGG6AeZHLzFFL3_beRtxcR3zzEmnr6RjbqnrbkzmDog2g3P784lAKZKtWHwYvdn1LHi1fAdlVeu_K-xaHSiWP9B6t1mcpp2CZ5ee-JM_hCi51XOsTXRR75OuDyB_Ieu9c7bsaoVlXS-2oUL6zDXCArjqeWy1EDyGxmbPaM_011I-Is1Ltdl8zFbfB3UOe9opxCCisSwxBbl9RTg9zlt9iN7Z4BxCibvwoQjrXPv4W91F3arEK10RZhDYRljPSJtdDt_TIUoMAdyLraTX7nmOZpZITdH7Y5guC3q1RWWCNKt-87qkfZxReceuWdL44_CTyo9dBxQUJMYqIfXo_bnfZhB-GTW7I_8y6QXktpyRsl2gz64EQX1TUE5v4TQiRg3WfB_MXmcxNoDAAVhR6fmXYh5BSwqH_ncTYhAiNdJN0nyYVjBNcn-ahPiD1kpYt_0nUwofP1ngpwPq1b_0QIEs4X624uSzXkXxUySJoLDtILdtE43mfpP6-puSm7vP_Ktm9Erd63aHS9j9yW-a2rg2b_UhX_jSiKonlNxTlkxdUnHI8CO9sFWSgnpl2DB6JqTweNqCp6H1lB6LyUrxn_-pzQA6wKdfmm3oWpfYptFLXqhbahuqbWCVX-m_yC-oEaTV7os_Ehb54siEs</value>
         </entry>
      </authorizationInfo>
      <authorizationType>Bearer</authorizationType>
   </authorizationRequest>
   <autoUpdateContent>true</autoUpdateContent>
   <connectionTimeout>0</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;contentType&quot;: &quot;multipart/form-data&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;,
  &quot;parameters&quot;: [
    {
      &quot;name&quot;: &quot;name&quot;,
      &quot;value&quot;: &quot;update name&quot;,
      &quot;type&quot;: &quot;Text&quot;,
      &quot;contentType&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;whatsapp&quot;,
      &quot;value&quot;: &quot;h083823045047&quot;,
      &quot;type&quot;: &quot;Text&quot;,
      &quot;contentType&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;birth_date&quot;,
      &quot;value&quot;: &quot;1999-29-03&quot;,
      &quot;type&quot;: &quot;Text&quot;,
      &quot;contentType&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;photo&quot;,
      &quot;value&quot;: &quot;C:\\Users\\HP\\Documents\\QA\\data.xlsx&quot;,
      &quot;type&quot;: &quot;File&quot;,
      &quot;contentType&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;bio&quot;,
      &quot;value&quot;: &quot;&quot;,
      &quot;type&quot;: &quot;Text&quot;,
      &quot;contentType&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;position&quot;,
      &quot;value&quot;: &quot;mobile dev&quot;,
      &quot;type&quot;: &quot;Text&quot;,
      &quot;contentType&quot;: &quot;&quot;
    }
  ]
}</httpBodyContent>
   <httpBodyType>form-data</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>multipart/form-data</value>
      <webElementGuid>cbffd92b-155e-4ab3-bf29-2ed7924c4fc5</webElementGuid>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIzIiwianRpIjoiNTNiMTA3YTAxMzkyZTU3ZTRjY2E5MTAwNzUzZDkzMjBjNTIyMDQ5ZDAxZjdlYmViNDVmMjVlZGRjNjhmMDVhNjU1NzlhYjYwODE2ODdhMzciLCJpYXQiOjE2OTI1ODg5ODIuMjQ4ODQxLCJuYmYiOjE2OTI1ODg5ODIuMjQ4ODQzLCJleHAiOjE3MjQyMTEzODIuMjQ3NjM5LCJzdWIiOiI2NCIsInNjb3BlcyI6W119.JX_LkPVLF4iQ3hjSzVw9DjYZpG1LHCBGG6AeZHLzFFL3_beRtxcR3zzEmnr6RjbqnrbkzmDog2g3P784lAKZKtWHwYvdn1LHi1fAdlVeu_K-xaHSiWP9B6t1mcpp2CZ5ee-JM_hCi51XOsTXRR75OuDyB_Ieu9c7bsaoVlXS-2oUL6zDXCArjqeWy1EDyGxmbPaM_011I-Is1Ltdl8zFbfB3UOe9opxCCisSwxBbl9RTg9zlt9iN7Z4BxCibvwoQjrXPv4W91F3arEK10RZhDYRljPSJtdDt_TIUoMAdyLraTX7nmOZpZITdH7Y5guC3q1RWWCNKt-87qkfZxReceuWdL44_CTyo9dBxQUJMYqIfXo_bnfZhB-GTW7I_8y6QXktpyRsl2gz64EQX1TUE5v4TQiRg3WfB_MXmcxNoDAAVhR6fmXYh5BSwqH_ncTYhAiNdJN0nyYVjBNcn-ahPiD1kpYt_0nUwofP1ngpwPq1b_0QIEs4X624uSzXkXxUySJoLDtILdtE43mfpP6-puSm7vP_Ktm9Erd63aHS9j9yW-a2rg2b_UhX_jSiKonlNxTlkxdUnHI8CO9sFWSgnpl2DB6JqTweNqCp6H1lB6LyUrxn_-pzQA6wKdfmm3oWpfYptFLXqhbahuqbWCVX-m_yC-oEaTV7os_Ehb54siEs</value>
      <webElementGuid>07b12007-f382-49bd-8e0a-17aa1a4e4baf</webElementGuid>
   </httpHeaderProperties>
   <katalonVersion>8.6.0</katalonVersion>
   <maxResponseSize>0</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>https://demo-app.online/api/updateprofile</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>0</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
