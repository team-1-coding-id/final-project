<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Login</name>
   <tag></tag>
   <elementGuidId>8cc43040-eb86-4f8f-bbd5-c593b22772fa</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#buttonLoginTrack</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='buttonLoginTrack']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>994f17d3-d91f-4c0f-bb6b-eeca20a36db7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>buttonLoginTrack</value>
      <webElementGuid>5dd9ed49-a75a-4066-b576-bab3164658a3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>submit</value>
      <webElementGuid>93416a5d-eae6-491b-8dfb-a8a01cac4cbb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary btn-lg btn-block</value>
      <webElementGuid>2891df8b-cac6-48f6-93d7-5242712bab51</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>4</value>
      <webElementGuid>51935ed5-5f42-4be9-8370-53c332e3c67f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                        Login
                                                    </value>
      <webElementGuid>96751854-583e-4b09-80f1-9fbf1bfcca37</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;buttonLoginTrack&quot;)</value>
      <webElementGuid>669f6122-289e-4ba4-8294-75885ef5edc9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='buttonLoginTrack']</value>
      <webElementGuid>4207d314-9c36-4f5a-8d2a-50ec45ac52c0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Email'])[1]/following::button[1]</value>
      <webElementGuid>44dbc0f4-8c37-407c-aece-d2c10520b6a1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lupa kata sandi ?'])[1]/preceding::button[2]</value>
      <webElementGuid>c7202674-527c-46aa-b727-918f65298e2d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/button</value>
      <webElementGuid>36ae58c4-ca35-481c-bf31-82d49fc04bea</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@id = 'buttonLoginTrack' and @type = 'submit' and (text() = '
                                                        Login
                                                    ' or . = '
                                                        Login
                                                    ')]</value>
      <webElementGuid>b720d51b-c442-4815-851e-3410964c6e45</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
