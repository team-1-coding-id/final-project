<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Total Pembayaran_payment_method</name>
   <tag></tag>
   <elementGuidId>defc01d3-271e-47f8-a65e-b279f888cecc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#bank</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='bank']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>ba507f4d-96cd-4401-a69f-540f2fdeae29</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>radio</value>
      <webElementGuid>52d13bb0-fbe0-4fa7-aaf7-70d2dad2692a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control-inline payment</value>
      <webElementGuid>92d8934a-393f-4d9f-bb35-eef1bce052cb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>payment_method</value>
      <webElementGuid>53219a04-d496-4869-bba6-48c423f47d3a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>e-wallet</value>
      <webElementGuid>4d1a45ef-033a-45f4-b658-4d912d259f5a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>bank</value>
      <webElementGuid>80d6d310-2d84-455a-9747-0bdb5324231f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;bank&quot;)</value>
      <webElementGuid>bd89feb3-e4f4-428a-be66-3fb0a4cda0be</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='bank']</value>
      <webElementGuid>1d00920b-7805-4a05-8879-fb44498b3432</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='modalform']/table[2]/tbody/tr/td/input</value>
      <webElementGuid>5efcfd07-d8db-49c5-9105-ace657698235</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td/input</value>
      <webElementGuid>f3711067-27ae-4188-82f7-c962cd0cdfcb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'radio' and @name = 'payment_method' and @id = 'bank']</value>
      <webElementGuid>31ee1125-52da-49f8-a416-94db80b2a428</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
