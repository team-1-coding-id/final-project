<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>iframe_concat(id(, , snap-midtrans, , ))_po_e42801</name>
   <tag></tag>
   <elementGuidId>bfc37c26-0eed-48be-918f-8261090a0808</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#snap-midtrans</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//iframe[@id='snap-midtrans']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>iframe</value>
      <webElementGuid>31d7771c-5d49-4f5e-867f-3b33b57622e7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>src</name>
      <type>Main</type>
      <value>https://app.sandbox.midtrans.com/snap/v1/pay?origin_host=https://demo-app.online&amp;digest=3a36c044bc782196e9f9d6e807f015618592a7dd1b1a1cc0e8532c33343ca639&amp;client_key=SB-Mid-client-aQuCzf_xly4hqcSY#/</value>
      <webElementGuid>81bb8ffa-78a3-46a6-9455-9fbb843eb17a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>snap-midtrans</value>
      <webElementGuid>515b20ab-15b2-410f-ab83-c964a23ee1fd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>popup_1691988881842</value>
      <webElementGuid>20cbc10d-8cac-430b-b84f-5b787abe67a0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;snap-midtrans&quot;)</value>
      <webElementGuid>c055d6c3-c4e9-45a5-87e5-7b482ff83817</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//iframe[@id='snap-midtrans']</value>
      <webElementGuid>2edd44ec-7307-405d-adbe-dbffc392b379</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//iframe</value>
      <webElementGuid>d7227dbc-56e4-4214-a43f-976165becc92</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//iframe[@src = 'https://app.sandbox.midtrans.com/snap/v1/pay?origin_host=https://demo-app.online&amp;digest=3a36c044bc782196e9f9d6e807f015618592a7dd1b1a1cc0e8532c33343ca639&amp;client_key=SB-Mid-client-aQuCzf_xly4hqcSY#/' and @id = 'snap-midtrans' and @name = 'popup_1691988881842']</value>
      <webElementGuid>d91b8d86-12e0-49a6-bc6c-04cd1c01e87e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
