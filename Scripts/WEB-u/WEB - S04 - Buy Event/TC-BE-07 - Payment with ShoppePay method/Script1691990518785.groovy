import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('https://demo-app.online/')

WebUI.takeScreenshot()

WebUI.click(findTestObject('Object Repository/WEB-u/shoppepay/Page_Be a Profressional Talent with Coding.ID/a_Masuk'))

WebUI.takeScreenshot()

WebUI.setText(findTestObject('Object Repository/WEB-u/shoppepay/Page_Masuk untuk dapatkan akses di Coding.ID/input_Email_email'), 
    'nasihulumam15@gmail.com')

WebUI.setEncryptedText(findTestObject('Object Repository/WEB-u/shoppepay/Page_Masuk untuk dapatkan akses di Coding.ID/input_Kata                                 _98da12'), 
    '34vTivbG0FSs7zzBkH57Ug==')

WebUI.takeScreenshot()

WebUI.click(findTestObject('Object Repository/WEB-u/shoppepay/Page_Masuk untuk dapatkan akses di Coding.ID/button_Login'))

WebUI.click(findTestObject('Object Repository/WEB-u/shoppepay/Page_Coding.ID - Cart/a_Events'))

WebUI.takeScreenshot()

WebUI.click(findTestObject('Object Repository/WEB-u/shoppepay/Page_Online event bersertifikat dari prakti_f42b96/div_Day 4 Workshop                         _31d43a_1'))

WebUI.click(findTestObject('Object Repository/WEB-u/shoppepay/Page_Day 4 Workshop - Ziyad/a_Beli                                     _1a5f60'))

WebUI.click(findTestObject('Object Repository/WEB-u/shoppepay/Page_Day 4 Workshop - Ziyad/a_Lihat                Pembelian Saya'))

WebUI.takeScreenshot()

WebUI.click(findTestObject('Object Repository/WEB-u/shoppepay/Page_Coding.ID - Cart/button_Checkout'))

WebUI.click(findTestObject('Object Repository/WEB-u/shoppepay/Page_Coding.ID - Cart/input_Total Pembayaran_payment_method'))

WebUI.click(findTestObject('Object Repository/WEB-u/shoppepay/Page_Coding.ID - Cart/button_Confirm'))

WebUI.takeScreenshot()

WebUI.click(findTestObject('Object Repository/WEB-u/shoppepay/Page_Coding.ID - Cart/div_ShopeePay'))

WebUI.takeScreenshot()

WebUI.closeBrowser()

